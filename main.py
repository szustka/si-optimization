from src.genetic.init_population.random_init_population import RandomInitPopulation
from src.genetic.selection.tournament_selection import TournamentSelection
from src.genetic.selection.roulette_selection import RouletteSelection
from src.genetic.crossover.simple_crossover import SimpleCrossover
from src.genetic.mutation.simple_mutation import SimpleMutation
from src.genetic.genetic_solver import GeneticSolver
from src.genetic.random_solver import RandomSolver
from src.read_file import read_problem_data
from src.tile_problem import TileProblem
import time

if __name__ == '__main__':
    problem_data = read_problem_data("./examples/zad1.txt")
    tile_problem = TileProblem.from_file_data(problem_data)

    random_solver = RandomSolver(
        tile_problem,
        iterations=10000
    )
    random_solver.set_init_population_strategy(RandomInitPopulation(
        min_segments=2,
        max_segments=6,
        min_segment_length=1,
        max_segment_length=6
    ))

    start = time.time()
    print(
        "Random:",
        random_solver.run(),
        str(round(time.time() - start, 2)) + "s",
    )

    genetic_solver = GeneticSolver(
        tile_problem,
        iterations=10000,
        population_size=10
    )
    genetic_solver.set_init_population_strategy(RandomInitPopulation(
        min_segments=2,
        max_segments=6,
        min_segment_length=1,
        max_segment_length=6
    ))
    # genetic_solver.set_selection_strategy(TournamentSelection(
    #     genotypes_to_roll=8
    # ))
    genetic_solver.set_selection_strategy(RouletteSelection(
        etha=10
    ))
    genetic_solver.set_crossover_strategy(SimpleCrossover(
        crossover_chance=0.8
    ))
    genetic_solver.set_mutation_strategy(SimpleMutation(
        mutation_chance=0.1,
        max_move_length=3
    ))

    start = time.time()
    print(
        "Genetic:",
        genetic_solver.run(),
        str(round(time.time() - start, 2)) + "s",
    )
