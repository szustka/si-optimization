from src.types import Point, PathData
import toolz.curried as tzc
import toolz as tz
import numpy as np


class TileProblem:
    INTERSECTIONS_COUNT_WEIGHT = 50
    STEPS_COUNT_WEIGHT = 5
    SEGMENTS_COUNT_WEIGHT = 5
    STEPS_OUT_COUNT_WEIGHT = 50
    PATHS_OUT_COUNT_WEIGHT = 50

    __width: int
    __height: int
    __paths_data: list

    def __init__(self, width: int, height: int, paths_data: list = None):
        self.__width = width
        self.__height = height
        self.__paths_data = [] if paths_data is None else list(paths_data)

    def append_path(self, start: Point, end: Point):
        self.__paths_data.append(PathData(start, end))

    def rate_solution(self, solution: list):
        intersections_count = 0
        steps_count = 0
        segments_count = 0
        steps_out_count = 0
        paths_out_count = 0

        matrix = np.zeros((self.__width, self.__height))

        for index, segments in enumerate(solution):
            path_id = index + 1
            path_data = self.paths[index]
            pointer = Point(x=path_data.start.x, y=path_data.start.y)
            matrix[pointer.x][pointer.y] = path_id
            is_path_out_counted = False

            for segment in segments:
                segments_count += 1

                for step in range(segment.length):
                    pointer.move(segment.direction)
                    steps_count += 1

                    if (
                            pointer.x < 0
                            or pointer.y < 0
                            or pointer.x >= self.__width
                            or pointer.y >= self.__height
                    ):
                        steps_out_count += 1
                        if not is_path_out_counted:
                            is_path_out_counted = True
                            paths_out_count += 1
                    else:
                        matrix[pointer.x][pointer.y] = path_id
                        if matrix[pointer.x][pointer.y] != 0:
                            intersections_count += 1

        return (
            TileProblem.INTERSECTIONS_COUNT_WEIGHT * intersections_count +
            TileProblem.STEPS_COUNT_WEIGHT * steps_count +
            TileProblem.SEGMENTS_COUNT_WEIGHT * segments_count +
            TileProblem.STEPS_OUT_COUNT_WEIGHT * steps_out_count +
            TileProblem.PATHS_OUT_COUNT_WEIGHT * paths_out_count
        )

    @property
    def paths(self):
        return self.__paths_data

    @classmethod
    def from_file_data(cls, file_data: list):
        size, *paths_data = file_data
        paths = tz.pipe(
            paths_data,
            tzc.map(lambda line: PathData(
                start=Point(x=line[0][0], y=line[0][1]),
                end=Point(x=line[1][0], y=line[1][1])
            )),
            list
        )

        return cls(size[0][0], size[1][0], paths)
