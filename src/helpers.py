import dataclasses

from src.types import Segment, Direction


def find(pred, items):
    return next(filter(pred, items), None)


def split_list(parts: int, items: list):
    k, m = divmod(len(items), parts)
    gen = (items[id * k + min(id, m):(id + 1) * k + min(id + 1, m)] for id in range(parts))
    return list(gen)


def simplify_path(path: list):
    new_path = []
    is_path_changed = False
    for segment in path:
        if len(new_path) and new_path[-1].direction == segment.direction:
            new_path[-1].length += segment.length
            is_path_changed = True
        elif segment.length != 0:
            new_path.append(segment)
        else:
            is_path_changed = True

    return new_path if is_path_changed else path


def copy_path(path: list):
    return list(map(dataclasses.replace, path))


def copy_solution(solution: list):
    return list(map(copy_path, solution))


def reverse_direction(direction: Direction):
    if direction == Direction.UP:
        return Direction.DOWN
    if direction == Direction.DOWN:
        return Direction.UP
    if direction == Direction.RIGHT:
        return Direction.LEFT
    if direction == Direction.LEFT:
        return Direction.RIGHT


path = [
    Segment(Direction.UP, 3),
    Segment(Direction.RIGHT, 5),
    Segment(Direction.DOWN, 1)
]
