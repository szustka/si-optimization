from dataclasses import dataclass
from enum import Enum


class Direction(Enum):
    UP = 0
    LEFT = 1
    DOWN = 2
    RIGHT = 3


@dataclass
class Segment:
    direction: Direction
    length: int

    def move(self, direction: Direction, length=1):
        if self.direction == Direction.UP:
            if direction == Direction.UP:
                self.length = self.length + length
            if direction == Direction.DOWN:
                self.length = self.length - length

        if self.direction == Direction.DOWN:
            if direction == Direction.UP:
                self.length = self.length - length
            if direction == Direction.DOWN:
                self.length = self.length + length

        if self.direction == Direction.RIGHT:
            if direction == Direction.RIGHT:
                self.length = self.length + length
            if direction == Direction.LEFT:
                self.length = self.length - length

        if self.direction == Direction.LEFT:
            if direction == Direction.RIGHT:
                self.length = self.length - length
            if direction == Direction.LEFT:
                self.length = self.length + length


@dataclass
class Point:
    x: int
    y: int

    def move(self, direction: Direction, length=1):
        if direction == Direction.UP:
            self.y = self.y - length
        if direction == Direction.RIGHT:
            self.x = self.x + length
        if direction == Direction.DOWN:
            self.y = self.y + length
        if direction == Direction.LEFT:
            self.x = self.x - length


@dataclass
class PathData:
    start: Point
    end: Point
