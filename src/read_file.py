from src.helpers import split_list
import toolz.curried as tzc
import toolz as tz


def read_file_lines(filename: str):
    return open(filename, "rt", encoding="utf-8").readlines()


def map_single_file_line(line: str):
    return tz.pipe(
        line.replace("\n", "").split(";"),
        tzc.map(int),
        list,
        tzc.curry(split_list)(2),
    )


def map_file_lines(lines: list):
    return tz.pipe(
        lines,
        tzc.map(map_single_file_line),
        list
    )


def read_problem_data(filename: str):
    return tz.pipe(
        filename,
        read_file_lines,
        map_file_lines
    )
