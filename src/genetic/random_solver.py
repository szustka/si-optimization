from src.tile_problem import TileProblem


class RandomSolver:
    __tile_problem: TileProblem
    __iterations: int

    __init_population_strategy = None

    def __init__(
            self,
            tile_problem: TileProblem,
            iterations=1000,
    ):
        self.__tile_problem = tile_problem
        self.__iterations = iterations

    def run(self):
        best_population = None
        best_rate = None

        for _ in range(self.__iterations):
            new_population = self.__run_init_population()
            new_rate = self.__tile_problem.rate_solution(
                new_population
            )
            if not best_rate or new_rate < best_rate:
                best_population = new_population
                best_rate = new_rate

        return best_rate

    def __run_init_population(self):
        return self.__init_population_strategy.run(self.__tile_problem)

    def set_init_population_strategy(self, init_population_strategy):
        self.__init_population_strategy = init_population_strategy
