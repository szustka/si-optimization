from src.genetic.crossover.simple_crossover import SimpleCrossover
from src.types import Segment, Direction

first_genotype = [
    [Segment(Direction.UP, 1), Segment(Direction.UP, 1)],
    [Segment(Direction.UP, 2), Segment(Direction.UP, 2)],
]

second_genotype = [
    [Segment(Direction.DOWN, 1), Segment(Direction.DOWN, 1)],
    [Segment(Direction.DOWN, 2), Segment(Direction.DOWN, 2)],
]

print(SimpleCrossover(
    crossover_chance=1
).run(first_genotype, second_genotype))
