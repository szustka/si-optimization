from src.helpers import copy_solution
import random


class SimpleCrossover:
    __crossover_chance: int

    def __init__(self, crossover_chance):
        self.__crossover_chance = crossover_chance

    def run(self, first_genotype: list, second_genotype: list):
        if random.random() > self.__crossover_chance:
            return first_genotype, second_genotype

        cross_point = random.randrange(len(first_genotype))
        first_child = first_genotype[:cross_point] + second_genotype[cross_point:]
        second_child = second_genotype[:cross_point] + first_genotype[cross_point:]
        return copy_solution(first_child), copy_solution(second_child)
