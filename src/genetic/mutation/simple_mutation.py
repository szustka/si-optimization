from src.helpers import simplify_path, copy_path, reverse_direction
from src.types import Direction, Segment
import random


class SimpleMutation:
    __mutation_chance: int
    __max_move_length: int

    def __init__(self, mutation_chance, max_move_length):
        self.__mutation_chance = mutation_chance
        self.__max_move_length = max_move_length

    def run(self, genotype: list):
        return list(map(self.__mutate_path, genotype))

    def __mutate_path(self, path):
        if random.random() > self.__mutation_chance:
            return path

        cloned_genotype = copy_path(path)
        cross_point = random.randrange(len(path))
        move_length = random.randrange(1, self.__max_move_length)
        move_direction = SimpleMutation.__roll_move_direction(
            cloned_genotype[cross_point].direction
        )

        if cross_point == 0:
            cloned_genotype.insert(0, Segment(
                move_direction,
                move_length
            ))
            cross_point += 1
        else:
            cloned_genotype[cross_point - 1].move(move_direction, move_length)

        if cross_point == len(cloned_genotype) - 1:
            cloned_genotype.append(Segment(
                reverse_direction(move_direction),
                move_length
            ))
        else:
            cloned_genotype[cross_point + 1].move(
                reverse_direction(move_direction),
                move_length
            )

        return simplify_path(cloned_genotype)

    @staticmethod
    def __roll_move_direction(direction: Direction):
        if direction == Direction.DOWN or direction == Direction.UP:
            return Direction.RIGHT if random.randint(0, 1) else Direction.LEFT
        return Direction.UP if random.randint(0, 1) else Direction.DOWN
