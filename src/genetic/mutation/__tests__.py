from src.genetic.mutation.simple_mutation import SimpleMutation
from src.types import Segment, Direction

genotype = [
    [
        Segment(Direction.UP, 3),
        Segment(Direction.RIGHT, 3),
        Segment(Direction.DOWN, 3)
    ],
]

print(SimpleMutation(
    mutation_chance=1,
    max_move_length=5
).run(genotype))
