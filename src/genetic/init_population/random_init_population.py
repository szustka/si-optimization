from src.types import Point, Segment, Direction, PathData
from src.tile_problem import TileProblem
import dataclasses
import random


class RandomInitPopulation:
    __min_segments = 2,
    __max_segments = 6,
    __min_segment_length = 1,
    __max_segment_length: int

    def __init__(
            self,
            min_segments=2,
            max_segments=6,
            min_segment_length=1,
            max_segment_length=6
    ):
        self.__min_segments = min_segments
        self.__max_segments = max_segments
        self.__min_segment_length = min_segment_length
        self.__max_segment_length = max_segment_length

    def run(self, problem: TileProblem):
        return list(map(
            self.__generate_random_path,
            problem.paths
        ))

    def __generate_random_path(self, path_data: PathData):
        path = []
        cur_point = dataclasses.replace(path_data.start)

        for _ in range(random.randint(
                self.__min_segments,
                self.__max_segments
        )):
            new_segment = Segment(
                direction=self.__get_random_direction(),
                length=random.randint(
                    self.__min_segment_length,
                    self.__max_segment_length
                )
            )
            path.append(new_segment)
            cur_point.move(
                new_segment.direction,
                new_segment.length
            )

        return path + RandomInitPopulation.__connect_points_by_path(
            cur_point,
            path_data.end
        )

    @staticmethod
    def __connect_points_by_path(start_point: Point, end_point: Point):
        x_delta = start_point.x - end_point.x
        x_direction = Direction.RIGHT if x_delta < 0 else Direction.LEFT
        x_segment = Segment(direction=x_direction, length=abs(x_delta))

        y_delta = start_point.y - end_point.y
        y_direction = Direction.DOWN if y_delta < 0 else Direction.UP
        y_segment = Segment(direction=y_direction, length=abs(y_delta))

        if x_delta == 0:
            return [y_segment]
        if y_delta == 0:
            return [x_segment]

        return [x_segment, y_segment] if random.randint(0, 1) else [y_segment, x_segment]

    @staticmethod
    def __get_random_direction():
        return random.choice([
            Direction.UP,
            Direction.RIGHT,
            Direction.DOWN,
            Direction.LEFT
        ])