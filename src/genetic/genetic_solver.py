from src.tile_problem import TileProblem


class GeneticSolver:
    __tile_problem: TileProblem
    __iterations: int
    __population_size: int

    __init_population_strategy = None
    __selection_strategy = None
    __crossover_strategy = None
    __mutation_strategy = None

    def __init__(
            self,
            tile_problem: TileProblem,
            iterations=1000,
            population_size=10
    ):
        self.__tile_problem = tile_problem
        self.__iterations = iterations
        self.__population_size = population_size

    def run(self):
        population = [
            self.__run_init_population()
            for _ in range(self.__population_size)
        ]

        for _ in range(self.__iterations):
            transitional_population = []

            while len(transitional_population) != self.__population_size:
                first_parent = self.__run_selection(population)
                second_parent = self.__run_selection(population)

                first_child, second_child = self.__run_crossover(first_parent, second_parent)
                mutated_first_child = self.__run_mutation(first_child)
                mutated_second_child = self.__run_mutation(second_child)
                transitional_population.append(mutated_first_child)
                transitional_population.append(mutated_second_child)

            population = transitional_population

        return min(list(map(
            self.__tile_problem.rate_solution,
            population
        )))

    def __run_init_population(self):
        return self.__init_population_strategy.run(self.__tile_problem)

    def __run_selection(self, population):
        return self.__selection_strategy.run(population, self.__tile_problem)

    def __run_crossover(self, first_parent, second_parent):
        return self.__crossover_strategy.run(
            first_parent,
            second_parent
        )

    def __run_mutation(self, genotype):
        if not self.__mutation_strategy:
            return genotype
        return self.__mutation_strategy.run(genotype)

    def set_init_population_strategy(self, init_population_strategy):
        self.__init_population_strategy = init_population_strategy

    def set_selection_strategy(self, selection_strategy):
        self.__selection_strategy = selection_strategy

    def set_crossover_strategy(self, crossover_strategy):
        self.__crossover_strategy = crossover_strategy

    def set_mutation_strategy(self, mutation_strategy):
        self.__mutation_strategy = mutation_strategy
