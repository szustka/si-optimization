from src.tile_problem import TileProblem
import random


class TournamentSelection:
    __genotypes_to_roll: int

    def __init__(self, genotypes_to_roll: int):
        self.__genotypes_to_roll = genotypes_to_roll

    def run(self, population: list, tile_problem: TileProblem):
        random_genotypes = random.sample(
            population,
            self.__genotypes_to_roll
        )
        sorted_genotypes = sorted(
            random_genotypes,
            key=tile_problem.rate_solution,
        )
        return sorted_genotypes[0]
