from src.genetic.selection.roulette_selection import RouletteSelection
from src.genetic.selection.tournament_selection import TournamentSelection
from src.tile_problem import TileProblem
from src.types import Segment, Direction, PathData, Point

tile_problem = TileProblem(width=5, height=5)
tile_problem.append_path(Point(0, 0), Point(5, 5))

first_solution = [
    [
        Segment(Direction.RIGHT, 5),
        Segment(Direction.DOWN, 5)
    ],
]

second_solution = [
    [
        Segment(Direction.RIGHT, 1),
        Segment(Direction.DOWN, 1),
        Segment(Direction.RIGHT, 1),
        Segment(Direction.DOWN, 1),
        Segment(Direction.RIGHT, 3),
        Segment(Direction.DOWN, 3)
    ],
]

population = [first_solution, second_solution]

print(TournamentSelection(
    genotypes_to_roll=1
).run(population, tile_problem))

print(RouletteSelection(
    etha=10
).run(population, tile_problem))
