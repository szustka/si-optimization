from src.tile_problem import TileProblem
import random


class RouletteSelection:
    __etha: int

    def __init__(self, etha=10):
        self.__etha = etha

    def run(self, population: list, tile_problem: TileProblem):
        population_rates = list(map(
            tile_problem.rate_solution,
            population
        ))
        worst_rate = max(population_rates)
        population_chances = list(map(
            lambda rate: worst_rate - rate + self.__etha,
            population_rates
        ))

        roll = random.randint(0, sum(population_chances))
        prev_rates_sum = 0
        for index, rate in enumerate(population_chances):
            if roll <= rate + prev_rates_sum:
                return population[index]
            prev_rates_sum += rate
